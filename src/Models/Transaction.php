<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Models;

class Transaction
{
    private User $user;
    private float $amount;
    private string $date;
    private string $type;
    private Currency $currency;

    public function __construct($amount, $date, $type)
    {
        $this->amount = $amount;
        $this->date = $date;
        $this->type = $type;
    }

    public function calculateFee()
    {
        foreach ($this->getUser()->getPlans() as $plan) {
            if ($plan->canApply($this)) {
                return $plan->calculateFee($this);
            }
        }
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
