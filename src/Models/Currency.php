<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Models;

use Bashcole\CommissionCalculator\Plans\Plan;

class Currency
{
    private string $code;
    private float $rate;

    public function __construct(string $code, $rate)
    {
        $this->code = $code;
        $this->rate = $rate;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getRate(): float
    {
        return $this->rate;
    }
}
