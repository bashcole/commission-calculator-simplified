<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Traits;

trait PreviousTransaction
{
    protected $previousTransaction;

    protected function setPreviousTransaction($transaction)
    {
        $this->previousTransaction = $transaction;
    }

    protected function getPreviousTransaction()
    {
        return $this->previousTransaction;
    }
}
