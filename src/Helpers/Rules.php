<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Helpers;

class Rules
{
    public static function in($value, $allowed): bool
    {
        return in_array($value, $allowed);
    }

    public static function isDate($value, $format = "Y-m-d"): bool
    {
        $d = \DateTime::createFromFormat($format, $value);
        return $d && $d->format($format) == $value;
    }

    public static function isInteger($value): bool
    {
        return is_int($value);
    }
}
