<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Traits;

trait Limit
{
    protected $maxLimit;

    protected function setLimit($limit)
    {
        $this->maxLimit = $limit;
    }

    protected function getLimit()
    {
        return $this->maxLimit;
    }

    protected function decreaseLimit($amount)
    {
        $this->maxLimit -= $amount;
        $this->maxLimit = max(0, $this->maxLimit);
    }
}
