<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Traits;

trait Threshold
{
    protected $maxThreshold;

    protected function setThreshold($threshold)
    {
        $this->maxThreshold = $threshold;
    }

    protected function getThreshold()
    {
        return $this->maxThreshold;
    }

    protected function decreaseThreshold($amount)
    {
        $this->maxThreshold -= $amount;
        $this->maxThreshold = max(0, $this->maxThreshold);
    }
}
