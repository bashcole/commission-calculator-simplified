<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Helpers;

class Math
{
    public static function round(float $fee, int $precision = 2): float
    {
        return ceil($fee * pow(10, $precision)) / pow(10, $precision);
    }

    public static function add(
        string $leftOperand,
        string $rightOperand,
        int $scale = 3
    ): string {
        return bcadd($leftOperand, $rightOperand, $scale);
    }

    public static function sub(
        string $leftOperand,
        string $rightOperand,
        int $scale = 3
    ): string {
        return bcsub($leftOperand, $rightOperand, $scale);
    }

    public static function mul(
        string $leftOperand,
        string $rightOperand,
        int $scale = 3
    ): string {
        return bcmul($leftOperand, $rightOperand, $scale);
    }

    public static function div(
        string $leftOperand,
        string $rightOperand,
        int $scale = 3
    ): string {
        return bcdiv($leftOperand, $rightOperand, $scale);
    }
}
