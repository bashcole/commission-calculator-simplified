<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Factories;

use Bashcole\CommissionCalculator\Helpers\Path;
use Bashcole\CommissionCalculator\Services\Readers\JSONReader;
use Bashcole\CommissionCalculator\Services\Readers\CSVReader;

class ReaderFactory
{
    /**
     * @throws Exception
     */
    public function createReader(string $filePath)
    {
        $fileExtenstion = Path::getExtension($filePath);

        switch ($fileExtenstion) {
            case "CSV":
                return new CSVReader($filePath, false, ",", [
                  "date",
                  "user_id",
                  "user_type",
                  "type",
                  "amount",
                  "currency",
                ]);
            case "JSON":
                return new JSONReader($filePath);
            default:
                throw new Exception("Error! Unknown reader type");
        }
    }
}
