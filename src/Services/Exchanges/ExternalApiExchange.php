<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Services\Exchanges;

use Bashcole\CommissionCalculator\Models\Currency;

class ExternalApiExchange implements Exchange
{
    private $rates;
    private $url = "https://developers.paysera.com/tasks/api/currency-exchange-rates";

    public function __construct()
    {
        $items = $this->fetch();

        foreach ($items["rates"] as $code => $rate) {
            $this->rates[] = new Currency($code, $rate);
        }
    }

    private function fetch()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);

        curl_close($ch);

        return json_decode($output, true);
    }

    public function getRates()
    {
        return $this->rates;
    }

    /**
     * @throws \Exception
     */
    public function getRate($code)
    {
        return $this->findCurrencyByCode($code);
    }

    /**
     * @throws \Exception
     */
    private function findCurrencyByCode(string $currencyCode)
    {
        $currency = current(
            array_filter($this->currencies, function ($currency) use ($currencyCode) {
                return $currency->getCode() === $currencyCode;
            })
        );

        if (!$currency) {
            throw new \Exception("Currency not found.");
        }

        return $currency;
    }
}
