<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Services\Validators;

use Bashcole\CommissionCalculator\Helpers\Rules;

class DefaultValidator implements Validator
{
    /**
     * @throws \Exception
     */
    public function validate($data): void
    {
        foreach ($data as $i => $d) {
            if (!Rules::isInteger($d["user_id"] ?? null)) {
                throw new \Exception(
                    "The \"user_id\" column should have a valid integer at element: $i"
                );
            }

            if (!Rules::isDate($d["date"] ?? null)) {
                throw new \Exception(
                    "The \"date\" column should have a valid date Y-m-d at element: $i"
                );
            }

            if (!Rules::in($d["user_type"] ?? null, ["private", "business"])) {
                throw new \Exception(
                    "The \"user_type\" column can be: private or business, at element: $i"
                );
            }

            if (!Rules::in($d["type"] ?? null, ["withdraw", "deposit"])) {
                throw new \Exception(
                    "The \"type\" column can be: withdraw or deposit at element: $i"
                );
            }

            if (!Rules::in($d["currency"] ?? null, ["EUR", "JPY", "USD"])) {
                throw new \Exception(
                    "The \"currency\" column can be: EUR, JPY, USD at element: $i"
                );
            }
        }
    }
}
