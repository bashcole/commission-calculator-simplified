<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Traits\Periods;

use Bashcole\CommissionCalculator\Constants;
use DateTime;

trait SameWeek
{
    public function in_range($firstDate, $secondDate): bool
    {
        $first = DateTime::createFromFormat(Constants::DATE_FORMAT, $firstDate);
        $second = DateTime::createFromFormat(Constants::DATE_FORMAT, $secondDate);

        return $first->format("oW") === $second->format("oW");
    }
}
