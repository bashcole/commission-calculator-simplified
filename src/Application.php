<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator;

use Bashcole\CommissionCalculator\Services\Readers\Reader;
use Bashcole\CommissionCalculator\Services\Exchanges\Exchange;
use Bashcole\CommissionCalculator\Services\Validators\Validator;
use Bashcole\CommissionCalculator\Models\Transaction;
use Bashcole\CommissionCalculator\Models\User;
use Bashcole\CommissionCalculator\Models\Currency;
use Bashcole\CommissionCalculator\Factories\UserFactory;

class Application
{
    private Reader $reader;
    private Validator $validator;
    private Exchange $exchange;
    private array $rawTransactions = [];
    private array $transactions = [];
    private array $users = [];
    private array $currencies = [];
    private array $plans = [];

    public function setReader(Reader $readerService): Application
    {
        $this->reader = $readerService;

        return $this;
    }

    private function setUsers()
    {
        $userFactory = new UserFactory();
        foreach ($this->rawTransactions as $_transaction) {
            $this->users[] = $userFactory->createUser($_transaction, $this->plans);
        }
    }

    public function validateFile()
    {
        $this->validator->validate($this->rawTransactions);
    }

    private function processFile()
    {
        $this->reader->read();
        $this->rawTransactions = $this->reader->getData();
    }

public function setExchange(Exchange $exchange)
{
    $this->exchange = $exchange;
}

    public function setCurrencies()
    {
        $this->currencies = $this->exchange->getRates();
    }

    public function setValidator(Validator $validator)
    {
        $this->validator = $validator;
    }

    public function setPlans(array $plans)
    {
        $this->plans = $plans;
    }

    public function process(): array
    {
        $this->processFile();
        if ($this->validator) {
            $this->validateFile();
        }
        $this->setUsers();

        $fees = [];
        foreach ($this->rawTransactions as $_transaction) {
            $transaction = new Transaction(
                (float) $_transaction["amount"],
                $_transaction["date"],
                $_transaction["type"]
            );
            $transaction->setUser(
                $this->findUserByID((int) $_transaction["user_id"])
            );
            $transaction->setCurrency(
                $this->findCurrencyByCode($_transaction["currency"])
            );

            $fees[] = $transaction->calculateFee();
        }

        return $fees;
    }

    public function findUserByID(int $userID): User
    {
        $user = current(
            array_filter($this->users, function ($user) use ($userID) {
                return $user->getID() === $userID;
            })
        );

        if (!$user) {
            throw new \Exception("User not found.");
        }

        return $user;
    }

    public function findCurrencyByCode(string $currencyCode)
    {
        $currency = current(
            array_filter($this->currencies, function ($currency) use ($currencyCode) {
                return $currency->getCode() === $currencyCode;
            })
        );

        if (!$currency) {
            throw new \Exception("Currency not found.");
        }

        return $currency;
    }

    public function addCurrency(Currency $currency)
    {
        $this->currencies[] = $currency;
    }
}
