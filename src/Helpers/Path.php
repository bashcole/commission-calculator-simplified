<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Helpers;

class Path
{
    public static function getExtension(string $path): string
    {
        return strtoupper(pathinfo($path, PATHINFO_EXTENSION));
    }
}
