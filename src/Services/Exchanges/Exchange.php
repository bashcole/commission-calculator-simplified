<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Services\Exchanges;

interface Exchange
{
    public function getRates();
}
