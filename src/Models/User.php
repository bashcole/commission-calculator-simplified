<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Models;

use Bashcole\CommissionCalculator\Plans\Plan;

class User
{
    private int $id;
    private string $type;
    private array $plans;
    private $transaction;

    public function __construct(int $id, string $type)
    {
        $this->id = $id;
        $this->type = $type;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function addPlan(Plan $plan)
    {
        $this->plans[] = $plan;
    }

    public function getPlans()
    {
        return $this->plans;
    }

    public function setPreviousTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    public function getPreviousTransaction()
    {
        return $this->transaction;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
