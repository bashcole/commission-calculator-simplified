<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Services\Readers;

use Bashcole\CommissionCalculator\Services\Readers\Reader;

class JSONReader implements Reader
{
    private string $path;
    /**
     * @var false|string
     */
    private $data;

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function read()
    {
        $this->data = file_get_contents($this->path);
    }

    /**
     * @throws \Exception
     */
    public function getData(): array
    {
        $parse = json_decode($this->data, true);
        if (JSON_ERROR_NONE === json_last_error()) {
            return $parse;
        }
        throw new \Exception("Error! Invalid json");
    }
}
