# Commission calculator simplified

Features of the current implementation:

- Validation of data
- Ability to use/add different data sources. The current files supported are `.csv`, `.json`
- Ability to use/add different plans for calculating numerous commission fees
- Ability to use/add different Exchange rate providers. Supported: `local file`, `external api`
- Ability to use/add different periods in plans. For example the rule about 1 week, can be easily changed to 1 month using the `SameMonth` **trait** instead of `SameWeek` **trait**

Steps to run:

Install dependencies with:

- run `composer install`

For test use:

- run `composer run test`
- run `php simplified.php input.csv` or `php simplified.php input.json`