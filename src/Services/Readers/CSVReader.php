<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Services\Readers;

use Bashcole\CommissionCalculator\Services\Readers\Reader;

class CSVReader implements Reader
{
    private array $rows = [];
    private $header = [];
    private $handle;
    private $delimiter;
    private bool $hasHeader;

    public function __construct(
        $path,
        $hasHeader = false,
        $delimiter = ",",
        $header = []
    ) {
        $this->hasHeader = $hasHeader;
        $this->delimiter = $delimiter;
        $this->header = $header;
        $this->handle = fopen($path, "r");
    }

    public function read()
    {
        $line = 0;

        while (($data = fgetcsv($this->handle, 1000, $this->delimiter)) !== false) {
            if ($this->hasHeader || !empty($this->header)) {
                $num = count($data);

                for ($c = 0; $c < $num; $c++) {
                    if ($line == 0 && $this->hasHeader) {
                        $this->header[] = strtolower($data[$c]);
                    } else {
                        $ceilTitle = $this->header[$c];

                        $this->rows[$line][$ceilTitle] = $this->normalize($data[$c]);
                    }
                }

                $line++;
            } else {
                $this->rows[] = $data;
            }
        }
    }

    public function setHeader(array $header)
    {
        $this->header = $header;
    }

    public function getHeader(): array
    {
        return $this->header;
    }

    public function getData(): array
    {
        return $this->rows;
    }

    public function normalize($value)
    {
        if (is_numeric($value) && strpos($value, ".") === false) {
            return (int) $value;
        }

        if (is_numeric($value)) {
            return (float) $value;
        }

        return $value;
    }
}
