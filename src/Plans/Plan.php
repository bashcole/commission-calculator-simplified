<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Plans;

use Bashcole\CommissionCalculator\Models\Transaction;

interface Plan
{
    public function canApply(Transaction $transaction): bool;
    public function calculateFee($transaction): float;
}
