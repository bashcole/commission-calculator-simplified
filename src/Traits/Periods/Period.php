<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\app\Periods;

interface Period
{
    public const DATE_FORMAT = 'Y-m-d';

    public function in_range(string $firstDate, string $secondDate): bool;
}
