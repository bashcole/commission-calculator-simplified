<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Tests;

use Bashcole\CommissionCalculator\Application;
use Bashcole\CommissionCalculator\Factories\ReaderFactory;
use Bashcole\CommissionCalculator\Helpers\Path;
use Bashcole\CommissionCalculator\Services\Validators\DefaultValidator;
use Bashcole\CommissionCalculator\Models\Currency;
use Bashcole\CommissionCalculator\Plans\DepositPlan;
use Bashcole\CommissionCalculator\Plans\WithdrawPlan;
use Bashcole\CommissionCalculator\Plans\PrivateWithdrawPlan;
use Bashcole\CommissionCalculator\Services\Exchanges\DummyApiExchange;

use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    public function dataProviderForCommissionTesting(): array
    {
        $input = __DIR__ . "/fixtures/input.csv";
        $commissionFees = [
          0.6,
          3.0,
          0.0,
          0.06,
          1.5,
          0.0,
          0.7,
          0.3,
          0.3,
          3.0,
          0.0,
          0.0,
          8611.41,
        ];

        return ["data from input.csv" => [$input, $commissionFees]];
    }

    /**
     * @param string $csvFilePath
     * @param array $expectedCommissionFees
     *
     * @dataProvider dataProviderForCommissionTesting
     */
    public function testApplication(
        string $csvFilePath,
        array $expectedCommissionFees
    ) {
        $readFactory = new ReaderFactory();
        $reader = $readFactory->createReader($csvFilePath);

        $validator = new DefaultValidator();
        $exchange = new DummyApiExchange();
        $app = new Application();

        $app->setReader($reader);
        $app->setValidator($validator);
        $app->setExchange($exchange);
        $app->setCurrencies();

        $app->setPlans([
          DepositPlan::class,
          WithdrawPlan::class,
          PrivateWithdrawPlan::class,
        ]);

        $this->assertEquals($expectedCommissionFees, $app->process());
    }
}
