<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Factories;

use Bashcole\CommissionCalculator\Models\User;

class UserFactory
{
    /**
     * @throws Exception
     */
    public function createUser($transaction, array $plans)
    {
        $user = new User((int) $transaction["user_id"], $transaction["user_type"]);
        foreach ($plans as $plan) {
            $user->addPlan(new $plan());
        }
        return $user;
    }
}
