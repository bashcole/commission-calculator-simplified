<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Services\Readers;

interface Reader
{
    public function read();
    public function getData(): array;
}
