<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Tests\Periods;

use Bashcole\CommissionCalculator\Traits\Periods\SameWeek;
use Bashcole\CommissionCalculator\Traits\Periods\SameMonth;
use PHPUnit\Framework\TestCase;

class PeriodTest extends TestCase
{
    /**
     * @param string $leftOperand
     * @param string $rightOperand
     * @param bool $expectation
     *
     * @dataProvider dataProviderForSameWeekTesting
     */
    public function testSameWeek(
        string $leftOperand,
        string $rightOperand,
        bool $expectation
    ) {
        $same_week = $this->getObjectForTrait(SameWeek::class);
        $this->assertEquals(
            $expectation,
            $same_week->in_range($leftOperand, $rightOperand)
        );
    }

    public function dataProviderForSameWeekTesting(): array
    {
        return [
          "2023-02-03 and 2023-02-05 are in the same week" => [
            "2023-02-03",
            "2023-02-05",
            true,
          ],
          "2023-02-03 and 2023-02-23 are not in the same week" => [
            "2023-02-03",
            "2023-02-23",
            false,
          ],
        ];
    }

    /**
     * @param string $leftOperand
     * @param string $rightOperand
     * @param bool $expectation
     *
     * @dataProvider dataProviderForSameMonthTesting
     */
    public function testSameMonth(
        string $leftOperand,
        string $rightOperand,
        bool $expectation
    ) {
        $same_month = $this->getObjectForTrait(SameMonth::class);
        $this->assertEquals(
            $expectation,
            $same_month->in_range($leftOperand, $rightOperand)
        );
    }

    public function dataProviderForSameMonthTesting(): array
    {
        return [
          "2022-02-03 and 2022-01-23 are not in the same month" => [
            "2022-02-03",
            "2022-01-23",
            false,
          ],
          "2022-02-03 and 2022-02-23 are in the same month" => [
            "2022-02-03",
            "2022-02-23",
            true,
          ],
        ];
    }
}
