<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Plans;

use Bashcole\CommissionCalculator\Constants;
use Bashcole\CommissionCalculator\Models\Transaction;
use Bashcole\CommissionCalculator\Helpers\Math;

class DepositPlan implements Plan
{
    private float $commission;

    public function __construct()
    {
        $this->commission = Constants::DEPOSIT_COMMISSION;
    }

    public function canApply(Transaction $transaction): bool
    {
        return $transaction->getType() === "deposit";
    }

    public function calculateFee($transaction): float
    {
        return Math::round(
            (float) Math::mul(
                (string) $transaction->getAmount(),
                (string) Constants::DEPOSIT_COMMISSION
            )
        );
    }
}
