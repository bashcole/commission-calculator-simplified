<?php

include __DIR__ . "/vendor/autoload.php";

use Bashcole\CommissionCalculator\Factories\ReaderFactory;
use Bashcole\CommissionCalculator\Services\Validators\DefaultValidator;
use Bashcole\CommissionCalculator\Plans\DepositPlan;
use Bashcole\CommissionCalculator\Plans\WithdrawPlan;
use Bashcole\CommissionCalculator\Application;
use Bashcole\CommissionCalculator\Services\Exchanges\ExternalApiExchange;
use Bashcole\CommissionCalculator\Plans\PrivateWithdrawPlan;

if (isset($argv[1]) === false) {
    die("Please provide a data source. Ex: input.csv, input.json");
}

$filePath = __DIR__ . "/public/data/" . $argv[1];

if (!file_exists($filePath)) {
    die("File doesn't exist");
}

try {
    $readFactory = new ReaderFactory();
    $reader = $readFactory->createReader($filePath);

    $validator = new DefaultValidator();
    $exchange = new ExternalApiExchange();
    $app = new Application();

    $app->setReader($reader);
    $app->setValidator($validator);
    $app->setExchange($exchange);
    $app->setCurrencies();

    $app->setPlans([
      DepositPlan::class,
      WithdrawPlan::class,
      PrivateWithdrawPlan::class,
    ]);

    $fees = $app->process();
    foreach ($fees as $fee) {
        echo $fee . PHP_EOL;
    }
} catch (Exception $exception) {
    die($exception->getMessage());
}
