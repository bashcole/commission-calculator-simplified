<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator;

class Constants
{
    public const DEPOSIT_COMMISSION = 0.0003;
    public const PRIVATE_WITHDRAW_COMMISSION = 0.003;
    public const BUSINESS_WITHDRAW_COMMISSION = 0.005;
    public const MAX_FREE_THRESHOLD = 1000;
    public const MAX_FREE_TRANSACTIONS = 3;
    public const DATE_FORMAT = "Y-m-d";
}
