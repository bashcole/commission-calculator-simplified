<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Services\Exchanges;

use Bashcole\CommissionCalculator\Models\Currency;

class LocalApiExchange implements Exchange
{
    private $rates;
    private $path = "/public/data/currency-exchange-rates.json";

    public function __construct()
    {
        $items = $this->fetch();
        foreach ($items["rates"] as $code => $rate) {
            $this->rates[] = new Currency($code, $rate);
        }
    }

    private function fetch()
    {
        $path = dirname(dirname(dirname(__DIR__))) . $this->path;

        $output = file_get_contents($path);

        return json_decode($output, true);
    }

    public function getRates()
    {
        return $this->rates;
    }

    /**
     * @throws \Exception
     */
    public function getRate($code)
    {
        return $this->findCurrencyByCode($code);
    }

    /**
     * @throws \Exception
     */
    private function findCurrencyByCode(string $currencyCode)
    {
        $currency = current(
            array_filter($this->currencies, function ($currency) use ($currencyCode) {
                return $currency->getCode() === $currencyCode;
            })
        );

        if (!$currency) {
            throw new \Exception("Currency not found.");
        }

        return $currency;
    }
}
