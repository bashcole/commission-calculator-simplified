<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Plans;

use Bashcole\CommissionCalculator\Constants;
use Bashcole\CommissionCalculator\Helpers\Math;
use Bashcole\CommissionCalculator\Traits\Limit;
use Bashcole\CommissionCalculator\Traits\Threshold;
use Bashcole\CommissionCalculator\Traits\PreviousTransaction;
use Bashcole\CommissionCalculator\Traits\Periods\SameWeek;
use Bashcole\CommissionCalculator\Models\Transaction;

class PrivateWithdrawPlan implements Plan
{
    use PreviousTransaction;
    use Limit;
    use Threshold;
    use SameWeek;

    private float $commission;

    public function __construct()
    {
        $this->commission = Constants::PRIVATE_WITHDRAW_COMMISSION;
        $this->setThreshold(Constants::MAX_FREE_THRESHOLD);
        $this->setLimit(Constants::MAX_FREE_TRANSACTIONS);
    }

    public function canApply(Transaction $transaction): bool
    {
        return $transaction->getUser()->getType() === "private" &&
          $transaction->getType() === "withdraw";
    }

    public function calculateFee($transaction): float
    {
        if ($this->shouldReset($transaction)) {
            $this->setThreshold(Constants::MAX_FREE_THRESHOLD);
            $this->setLimit(Constants::MAX_FREE_TRANSACTIONS);
        }

        $euroAmount = Math::round(
            $transaction->getAmount() / $transaction->getCurrency()->getRate()
        );

        $euroFeeAmount = $this->getFeeAmount($euroAmount);

        $feeAmount = round(
            $euroFeeAmount * $transaction->getCurrency()->getRate()
        );

        $this->decreaseThreshold($euroAmount);
        $this->decreaseLimit(1);

        $this->setPreviousTransaction($transaction);

        $fee = Math::mul(
            (string) $feeAmount,
            (string) Constants::PRIVATE_WITHDRAW_COMMISSION
        );

        return Math::round((float) $fee);
    }

    private function shouldReset($transaction): bool
    {
        if (empty($this->getPreviousTransaction())) {
            return false;
        }

        $areDatesInSameWeek = $this->in_range(
            $transaction->getDate(),
            $this->getPreviousTransaction()->getDate()
        );

        return !$areDatesInSameWeek;
    }

    private function getFeeAmount($amount)
    {
        if ($this->getThreshold() > $amount && $this->getLimit() > 0) {
            return 0.00;
        }
        return abs(
            (float) Math::sub((string) $this->getThreshold(), (string) $amount)
        );
    }
}
