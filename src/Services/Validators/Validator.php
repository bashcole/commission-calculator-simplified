<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Services\Validators;

interface Validator
{
    public function validate(array $data): void;
}
