<?php

declare(strict_types=1);

namespace Bashcole\CommissionCalculator\Plans;

use Bashcole\CommissionCalculator\Constants;
use Bashcole\CommissionCalculator\Models\Transaction;
use Bashcole\CommissionCalculator\Helpers\Math;

class WithdrawPlan implements Plan
{
    private float $commission;

    public function __construct()
    {
        $this->commission = Constants::BUSINESS_WITHDRAW_COMMISSION;
    }

    public function canApply(Transaction $transaction): bool
    {
        return $transaction->getUser()->getType() === "business" &&
          $transaction->getType() === "withdraw";
    }

    public function calculateFee($transaction): float
    {
        return Math::round(
            (float) Math::mul(
                (string) $transaction->getAmount(),
                (string) Constants::BUSINESS_WITHDRAW_COMMISSION
            )
        );
    }
}
